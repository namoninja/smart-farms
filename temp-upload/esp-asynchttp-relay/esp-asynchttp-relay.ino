//----------- INCLUDES -------------
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>

//------------ OBJECTS -------------
AsyncWebServer server(80);

//----------- DEFINITIONS ----------
#define RELAY_PIN 14

const char* ssid      = "WIFI_SSID";
const char* password  = "";

bool RELAY_STATE = 0;
String RELAY_STATUS = "Off";
String RELAY_KEY = "PUMP_PASSWORD";

//----------- SETUP ----------
void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(RELAY_PIN, OUTPUT);
  checkWifi();
  
  server.on("/", HTTP_GET, [] (AsyncWebServerRequest *request){

    AsyncWebServerResponse *response;

    if ( request->hasParam("relay") && request->getParam("key")->value() == RELAY_KEY ) {
      toggleRelay();
      response = request->beginResponse(200, "application/json", "{\"status\":\"" + RELAY_STATUS + "\"}");
    } else if ( request->hasParam("relay") && request->getParam("key")->value() != RELAY_KEY ) {
      response = request->beginResponse(401, "application/json", "{\"status\":\"" + RELAY_STATUS + "\"}");
    } else {
      response = request->beginResponse(200, "application/json", "{\"status\":\"" + RELAY_STATUS + "\"}");
    }
    response->addHeader("Access-Control-Allow-Origin","*");
    request->send(response);
    
  });
  
  server.begin();
}

//----------- LOOP ----------
void loop() 
{
  checkWifi();
  ledBlink();
  yield(); // Stabilize Watchdog
  delay(15000);
}

//----------- Functions ----------
void checkWifi()
{
  if ( WiFi.status() != WL_CONNECTED ) {
    RELAY_STATE = 0;
    WiFi.begin(ssid, password);
  
    while(WiFi.status() != WL_CONNECTED) { 
      delay(500);
    }
  }
}

void toggleRelay()
{
  RELAY_STATE = !RELAY_STATE;
  digitalWrite(RELAY_PIN, RELAY_STATE);
  RELAY_STATUS = RELAY_STATE ? "On" : "Off";
}

void ledBlink()
{
  digitalWrite(LED_BUILTIN, LOW);
  delay(100);
  digitalWrite(LED_BUILTIN, HIGH);
}
