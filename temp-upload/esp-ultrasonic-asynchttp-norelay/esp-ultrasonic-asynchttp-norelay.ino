//----------- INCLUDES -------------
#include <ESP8266WiFi.h>
#include <AsyncHttpClient.h>

//------------ OBJECTS -------------
AsyncHttpClient Client;

//----------- DEFINITIONS ----------
#define TRIG_PIN 5
#define ECHO_PIN 4

const char* ssid      = "WIFI_SSID";
const char* password  = "";

String HOST = "http://192.168.100.100/sensors/esp-data-post.php";
String API_KEY = "APIKEY";
String sensorName = "sonic1-rainwater";
String sensorLocation = "Mainhouse Header";

//----------- SETUP ----------
void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);

  checkWifi();
}

//----------- LOOP ----------
void loop() 
{
  checkWifi();
  String postData = "api_key=" + API_KEY + "&sensor=" + sensorName + "&location=" + sensorLocation + "&value=" + String(getDistance()) + "";
  Client.init("POST", HOST, "application/x-www-form-urlencoded", postData);
  Client.send();
  ledBlink();

  yield(); // Stabilize Watchdog
  delay(15000);
}

//----------- Functions ----------
float getDistance()
{
  long duration, distance;
  
  digitalWrite(TRIG_PIN, LOW); // Stabilize
  delay(10);
  digitalWrite(TRIG_PIN, HIGH);
  delay(10);
  digitalWrite(TRIG_PIN, LOW);

  duration = pulseIn(ECHO_PIN, HIGH);
  return duration * 0.034 / 2;
}

void checkWifi()
{
  if ( WiFi.status() != WL_CONNECTED ) {
    WiFi.begin(ssid, password);
  
    while(WiFi.status() != WL_CONNECTED) { 
      delay(500);
    }
  }
}

void ledBlink()
{
  digitalWrite(LED_BUILTIN, LOW);
  delay(100);
  digitalWrite(LED_BUILTIN, HIGH);
}
