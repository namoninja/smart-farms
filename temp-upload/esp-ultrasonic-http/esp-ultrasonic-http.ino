//----------- INCLUDES ----------
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include <WiFiClient.h>
WiFiClient client;
ESP8266WebServer server(80);

//----------- VARS ----------
#define TRIG_PIN 4
#define ECHO_PIN 5
#define RELAY_PIN 14

const char* ssid      = "WIFI_SSID";
const char* password  = "";
const char* host      = "http://192.168.100.100/sensors/esp-data-post.php";

String apiKeyValue = "APIKEY";
String sensorName = "Ultrasonic1";
String sensorLocation = "Rainwater";

bool RELAY_STATE = LOW;

//----------- SETUP ----------
void setup() {
  
  digitalWrite(LED_BUILTIN, LOW);
  digitalWrite(RELAY_PIN, LOW);
  
  Serial.begin(115200);
  
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(RELAY_PIN, OUTPUT);

  connectWifi();

  digitalWrite(LED_BUILTIN, HIGH);
  
  server.on("/toggle", toggleRelay);
  server.begin();
}

//----------- LOOP ----------
void loop() {
  
  delay(1);
  server.handleClient();
  
  float distance = readDistance();
  
  //--------- CHECK WIFI STATUS
  if ( WiFi.status() != WL_CONNECTED ) {
    Serial.println("WiFi Disconnected");
    connectWifi();
  }
    
  HTTPClient http;
  http.begin(client, host);
  http.addHeader("Content-Type", "application/x-www-form-urlencoded");
  
  String httpRequestData = "api_key=" + apiKeyValue + "&sensor=" + sensorName + "&location=" + sensorLocation + "&value=" + String(distance) + "";
                        
  Serial.print("httpRequestData: ");
  Serial.println(httpRequestData);

  // Send HTTP POST request
  int httpResponseCode = http.POST(httpRequestData);
      
  if (httpResponseCode>0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
  } else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  
  http.end(); // Free resources
  
  digitalWrite(LED_BUILTIN, LOW);
  delay(500);
  digitalWrite(LED_BUILTIN, HIGH);
  //delay(60000); //Send an HTTP POST request every 60 seconds
  
}

//----------- Functions ----------
float readDistance() {
  
  long duration;
  long current_micros = micros();

  digitalWrite(TRIG_PIN, HIGH);
  while( micros() - current_micros < 10 ) { }
  digitalWrite(TRIG_PIN, LOW);
  
  duration = pulseIn(ECHO_PIN, HIGH);
  return duration * 0.034f / 2.0f; // Speed of sound wave divided by 2 (go and back)
}

void connectWifi() {
  
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  
  while(WiFi.status() != WL_CONNECTED) { 
    delay(500);
    Serial.print(".");
  }
  
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}

void toggleRelay() {

  RELAY_STATE = !RELAY_STATE;
  digitalWrite(RELAY_PIN, RELAY_STATE);
  server.send(200, "text/plain", "Relay toggled");

}
