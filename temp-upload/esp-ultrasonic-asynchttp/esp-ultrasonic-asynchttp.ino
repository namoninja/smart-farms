//----------- INCLUDES -------------
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncHttpClient.h>

//------------ OBJECTS -------------
AsyncWebServer server(80);
AsyncHttpClient Client;

//----------- DEFINITIONS ----------
#define TRIG_PIN 4
#define ECHO_PIN 5
#define RELAY_PIN 16

const char* ssid      = "WIFI_SSID";
const char* password  = "";

String HOST = "http://192.168.100.100/sensors/esp-data-post.php";
String API_KEY = "APIKEY";
String sensorName = "sonic1-damwater";
String sensorLocation = "Resevoir Header";

bool RELAY_STATE = 0;
String RELAY_STATUS = "Off";
String RELAY_KEY = "PUMP_PASSWORD";

//----------- SETUP ----------
void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
  pinMode(RELAY_PIN, OUTPUT);

  checkWifi();
  
  server.on("/", HTTP_GET, [] (AsyncWebServerRequest *request){

    AsyncWebServerResponse *response;

    if ( request->hasParam("relay") && request->getParam("key")->value() == RELAY_KEY ) {
      toggleRelay();
      response = request->beginResponse(200, "application/json", "{\"status\":\"" + RELAY_STATUS + "\"}");
    } else if ( request->hasParam("relay") && request->getParam("key")->value() != RELAY_KEY ) {
      response = request->beginResponse(401, "application/json", "{\"status\":\"" + RELAY_STATUS + "\"}");
    } else {
      response = request->beginResponse(200, "application/json", "{\"status\":\"" + RELAY_STATUS + "\"}");
    }
    response->addHeader("Access-Control-Allow-Origin","*");
    request->send(response);
    
  });
  
  server.begin();
}

//----------- LOOP ----------
void loop() 
{
  checkWifi();
  String postData = "api_key=" + API_KEY + "&sensor=" + sensorName + "&location=" + sensorLocation + "&value=" + String(getDistance()) + "";
  Client.init("POST", HOST, "application/x-www-form-urlencoded", postData);
  Client.send();
  ledBlink();

  yield(); // Stabilize Watchdog
  delay(15000);
}

//----------- Functions ----------
float getDistance()
{
  long duration, distance;
  
  digitalWrite(TRIG_PIN, LOW); // Stabilize
  delay(10);
  digitalWrite(TRIG_PIN, HIGH);
  delay(10);
  digitalWrite(TRIG_PIN, LOW);

  duration = pulseIn(ECHO_PIN, HIGH);
  return duration * 0.034 / 2;
}

void checkWifi()
{
  if ( WiFi.status() != WL_CONNECTED ) {
    RELAY_STATE = 0;
    WiFi.begin(ssid, password);
  
    while(WiFi.status() != WL_CONNECTED) { 
      delay(500);
    }
  }
}

void toggleRelay()
{
  RELAY_STATE = !RELAY_STATE;
  digitalWrite(RELAY_PIN, RELAY_STATE);
  RELAY_STATUS = RELAY_STATE ? "On" : "Off";
}

void ledBlink()
{
  digitalWrite(LED_BUILTIN, LOW);
  delay(100);
  digitalWrite(LED_BUILTIN, HIGH);
}
