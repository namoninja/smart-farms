Smart Farms
###########

Welcome to the Smart Farms project.

This project is based off of a real life setup and extends via a combination of other environments. It is focused on an off-grid farm environment with a retreat center accomodating visitors.

Internet & Wifi is extended to accomodate visitors & long term residents, emulating an internal ISP.

Power is generated via solar energy.

Water is a combination of Dam, Rain & Borehole sources. It is heated via Donkeys, Passive solar, Compost.

Gas is generated via Biogas (Anaerobic Digestion).

Overview
########

In this example we have an off-grid farm of xHectares. It has multiple systems in place... here is an overview:

Intranet Components
###################

* RaspberryPi Model B 8GB RAM
* Mikrotik Routerboard
* Ubiquiti / Long Range Wifi CPEs
* Generic Switch

IoT Components
##############

These are some of the IoT components in use with their documentations.

Wireless:
* ESP8266-12F (Wifi / Microcontroller)

AC/DC:
* XXXXX (Relay Switch)
* XXXXX (VAC-VDC3.3v converter)
* XXXXX (Amperage Sensor)

Water:
* XXXXX (Water Guage sensor)
* XXXXX (Waterproof Ultrasonic Distance Sensor)
* XXXXX (Flow Rate Sensor)

Environmental:
* XXXXX (CO2 sensor)
* XXXXX (Humidity sensor)
* XXXXX (Gas Sensor)
* XXXXX (Dampness Sensor)
* XXXXX (Temperature Sensor)
* XXXXX (Lightning strike detector)
* XXXXX (Light sensor)
* XXXXX (Wind speed sensor)